import { User } from "../users/user.entity";
import { Entity, Column, PrimaryGeneratedColumn, ManyToOne, BeforeInsert, BeforeUpdate } from "typeorm";

@Entity()
export class Report {

  @BeforeInsert()
  @BeforeUpdate()
  toLowerCase() {
    this.make = this.make.toLowerCase();
    this.model = this.model.toLowerCase();
  }

  @PrimaryGeneratedColumn()
  id: number;

  @Column({ default: false })
  approved: boolean;

  @Column({ nullable: false })
  price: number;

  @Column({ nullable: false })
  make: string;

  @Column({ nullable: false })
  model: string;

  @Column({ nullable: false })
  year: number;

  @Column()
  lng: number;

  @Column()
  lat: number;

  @Column()
  mileage: number;

  @ManyToOne(
    () => User,
    (user) => user.reports,
    { nullable: false, onDelete: "CASCADE" }
  )
  user: User;

  @Column()
  userId: number;
}

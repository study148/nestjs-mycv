import {
  IsNumber,
  IsString,
  IsNotEmpty,
  MinLength,
  Min,
  Max,
  IsLongitude,
  IsLatitude
} from "class-validator";
import { Transform } from "class-transformer";

export class GetEstimateDto {
  @IsString()
  @MinLength(2)
  make: string;

  @IsString()
  @MinLength(1)
  model: string;

  @Transform(({ value }) => parseInt(value))
  @IsNumber({
    maxDecimalPlaces: 0
  })
  @Min(1900)
  @Max(new Date().getFullYear())
  year: number;

  @IsNotEmpty()
  @IsLongitude()
  lng: number;

  @IsNotEmpty()
  @IsLatitude()
  lat: number;

  @IsNotEmpty()
  @Transform(({ value }) => parseInt(value))
  @IsNumber({
    allowNaN: false,
    maxDecimalPlaces: 0
  })
  @Min(0)
  @Max(1000000)
  mileage: number;
}

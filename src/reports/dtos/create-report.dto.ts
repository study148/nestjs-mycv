import {
  IsNumber,
  IsString,
  IsNotEmpty,
  MinLength,
  Min,
  Max,
  IsLongitude,
  IsLatitude
} from "class-validator";

export class CreateReportDto {
  @IsNotEmpty()
  @IsNumber({
    allowNaN: false,
    maxDecimalPlaces: 2
  })
  @Min(100)
  @Max(1000000)
  price: number;

  @IsString()
  @MinLength(2)
  make: string;

  @IsString()
  @MinLength(1)
  model: string;

  @IsNumber({
    maxDecimalPlaces: 0
  })
  @Min(1900)
  @Max(new Date().getFullYear())
  year: number;

  @IsNotEmpty()
  @IsLongitude()
  lng: number;

  @IsNotEmpty()
  @IsLatitude()
  lat: number;

  @IsNotEmpty()
  @IsNumber({
    allowNaN: false,
    maxDecimalPlaces: 0
  })
  @Min(0)
  @Max(1000000)
  mileage: number;
}

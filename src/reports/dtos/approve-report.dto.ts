import { IsNotEmpty, IsBoolean } from "class-validator";

export class ApproveReportDto {
  @IsNotEmpty()
  @IsBoolean()
  approved: boolean;
}

import { Test, TestingModule } from "@nestjs/testing";
import { BadRequestException, NotFoundException } from "@nestjs/common";
import { AuthService } from "./auth.service";
import { UsersService } from "./users.service";
import { User } from './user.entity';

describe('AuthService', () => {
  let service: AuthService;
  let fakeUsersService: Partial<UsersService>;

  beforeEach(async () => {
    // Crate a fake copy of the users service
    let lastId = 0;
    const users: User[] = [];
    fakeUsersService = {
      find: (email: string) => {
        const filteredUsers = users.filter(user => user.email === email);
        return Promise.resolve(filteredUsers);
      },
      create: (email: string, password: string) => {
        lastId++;
        const user = { id: lastId, email: email, password: password } as User;
        users.push(user);
        return Promise.resolve(user);
      }
    };

    const module: TestingModule = await Test.createTestingModule({
      providers: [
        AuthService,
        {
          provide: UsersService,
          useValue: fakeUsersService
        }
      ]
    }).compile();

    service = module.get<AuthService>(AuthService);
  });

  it('can create an instance of auth service', async () => {
    expect(service).toBeDefined();
  });

  it('creates a new user with a salted and a hashed password', async () => {
    const user = await service.signUp('test@test.com', '1234');
    expect(user.password).not.toEqual('1234');
    const [salt, hash] = user.password.split('.');
    expect(salt).toBeDefined();
    expect(hash).toBeDefined();
  });

  it('it throws an error if user signs up with email that is in use', async () => {
    await service.signUp('exists@mail.com', '4321');

    expect.assertions(2);

    try {
      await service.signUp('exists@mail.com', '1234');
    } catch (err) {
      expect(err).toBeInstanceOf(BadRequestException);
      expect(err.message).toBe('email in use');
    }
  });

  it('throws if signin is called with an unused email', async () => {
    expect.assertions(2);
    try {
      await service.signIn('notexists@test.com', '1234');
    } catch (err) {
      expect(err).toBeInstanceOf(NotFoundException);
      expect(err.message).toBe('user not found');
    }
  });

  it('throws if an invalid password is provided', async () => {
    await service.signUp('user@mail.com', '1234');
    expect.assertions(2);

    try {
      await service.signIn('user@mail.com', '4321');
    } catch (err) {
      expect(err).toBeInstanceOf(BadRequestException);
      expect(err.message).toBe('bad password');
    }
  });

  it('returns a user if correct password is provided', async () => {
    await service.signUp('user@mail.com', '1234');

    const user = await service.signIn('user@mail.com', '1234');
    expect(user).not.toBeNull();
    expect(user).toBeDefined();
    expect(user.email).toBe('user@mail.com');
  });
});

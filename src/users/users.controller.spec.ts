import { Test, TestingModule } from '@nestjs/testing';
import { UsersController } from './users.controller';
import { UsersService } from './users.service';
import { AuthService } from './auth.service';
import { User } from './user.entity';
import { CreateUserDto } from './dtos/create-user.dto';

describe('UsersController', () => {
  let controller: UsersController;
  let fakeUsersService: Partial<UsersService>;
  let fakeAuthService: Partial<AuthService>;

  beforeEach(async () => {
    fakeUsersService = {
      findOne: (id: number) => {
        return Promise.resolve({ id, email: 'user@mail.com', password: '1234' } as User);
      },
      find: (email: string) => {
        return Promise.resolve([{ id: 1, email, password: '1234' } as User]);
      },
      // remove: () => { },
      // update: () => { }
    };
    fakeAuthService = {
      signIn: (email: string, password: string) => {
        return Promise.resolve({ id: 1, email, password } as User);
      },
      // signUp: () => { }
    };

    const module: TestingModule = await Test.createTestingModule({
      controllers: [UsersController],
      providers: [
        {
          provide: UsersService,
          useValue: fakeUsersService
        },
        {
          provide: AuthService,
          useValue: fakeAuthService
        }
      ]
    }).compile();

    controller = module.get<UsersController>(UsersController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('findAllUsers returns a list of user with the given email', async () => {
    const users = await controller.findAllUsers('user@mail.com');
    expect(users.length).toEqual(1);
    expect(users[0].email).toEqual('user@mail.com');
  });

  it('findUser returns a single user with the given ID', async () => {
    const user = await controller.findUser('1');
    expect(user).toBeDefined();
  });

  it('signIn updates session object and returns user', async () => {
    const session = { userId: undefined };
    const user = await controller.signIn({ email: 'user@mail.com', password: '1234' } as CreateUserDto, session);
    expect(user).toBeDefined();
    expect(session.userId).toBeDefined();
    expect(user.id).toEqual(1);
    expect(session.userId).toEqual(1);
  });
});
